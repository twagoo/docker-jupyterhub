#!/usr/bin/python3
# -*- coding: utf-8 -*-
import dbm
import bcrypt
import json
import os
import os.path

users_db_file = os.environ.get('HUB_USERS_PASSWORD_FILE')
users_json = os.environ.get('JUPYTER_HUB_CREDENTIALS')

if os.path.exists(f'{users_db_file}.db'):
	print(f'User database already exists, skipping initalisation (HUB_USERS_PASSWORD_FILE={users_db_file})')
else:
	print(f'Initalising user database (HUB_USERS_PASSWORD_FILE={users_db_file})')

	if users_json:
		print('Default user credentials defined in JUPYTER_HUB_CREDENTIALS')
		users = json.loads(users_json)
	else:
		print('WARNING: No default user credentials set!')
		users = {}

	with dbm.open(users_db_file, 'n') as db:
		for username in users:
			password = users[username]
			print(f'Adding user "{username}"')
			db[username] = bcrypt.hashpw(password.encode("utf8"), bcrypt.gensalt())
