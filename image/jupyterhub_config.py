import os
import json

c.JupyterHub.spawner_class = "dockerspawner.DockerSpawner"
c.DockerSpawner.image = os.environ["DOCKER_JUPYTER_IMAGE"]
c.DockerSpawner.network_name = os.environ["DOCKER_NETWORK_NAME"]
c.JupyterHub.hub_ip = os.environ["HUB_IP"]
c.Authenticator.admin_users = { os.environ.get('JUPYTER_HUB_ADMIN_USER') or 'admin'}

users_json = os.environ.get('JUPYTER_HUB_CREDENTIALS')
if users_json:
	users = json.loads(users_json)
	c.Authenticator.allowed_users = set(users.keys())

# from oauthenticator.github import GitHubOAuthenticator
# c.JupyterHub.authenticator_class = GitHubOAuthenticator
# 
# c.GitHubOAuthenticator.oauth_callback_url = \
#                     'http://<host_ip_addr>/hub/oauth_callback'
# c.GitHubOAuthenticator.client_id = '<client_id>'
# c.GitHubOAuthenticator.client_secret = '<client_secret>'

# from jupyterhub.auth import DummyAuthenticator
# c.JupyterHub.authenticator_class = DummyAuthenticator

from firstuseauthenticator import FirstUseAuthenticator
c.JupyterHub.authenticator_class = FirstUseAuthenticator
c.FirstUseAuthenticator.dbm_path = os.environ.get('HUB_USERS_PASSWORD_FILE')
c.FirstUseAuthenticator.create_users = False

notebook_dir = os.environ.get('DOCKER_NOTEBOOK_DIR') or '/home/jovyan/work'
c.DockerSpawner.notebook_dir = notebook_dir

data_volume = os.environ.get('DATA_VOLUME') or 'jupyterhub-data'
shared_volume = os.environ.get('SHARED_VOLUME') or 'jupyterhub-shared'

# Mount the real user's Docker volume on the host to the notebook user's
# notebook directory in the container
c.DockerSpawner.volumes = {
          'jupyterhub-user-{username}': notebook_dir,
          f'{shared_volume}': '/home/jovyan/work/shared',
          f'{data_volume}': {'bind': '/home/jovyan/work/data', 'mode': 'ro'}
}

c.DockerSpawner.remove_containers = True
c.Spawner.default_url = '/lab'

#TODO c.Spawner.mem_limit = '2G'